# seccomp_reproducer

Fails on Docker 20.10, workaround is disabling seccomp:

```
docker run --rm -it --security-opt seccomp=unconfined gitlab-registry.cern.ch/linuxsupport/cs9-base:20211011-1
```
